#!/usr/bin/env bash

if [ ! -d '/var/log/short_url' ]; then
    mkdir /var/log/short_url
    chown root /var/log/short_url
fi

if [ ! -f '/etc/logrotate.d/short_url' ]; then
    echo "/var/log/short_url/*.log {
        su root root
        daily
        missingok
        rotate 5
        copytruncate
        compress
        delaycompress
        size 2M
    }" >> /etc/logrotate.d/short_url
fi

logrotate -fv /etc/logrotate.d/short_url
