# SHORT_URL

Web application, that creates for long URL its short analogues.
For testing use the postman collection.
## FIX IT:

### init.sh
```
change path sudo ln -s {PATH}short_url/conf.d/nginx/short_url.web-dev.conf
```

### conf.d/nginx/short_url.web-dev.conf
```
1. change path in location
2. change server name short-url.dev41359.it-o.ru
```

### short_url/settings
```
change BASE_URL, ALLOWED_ORIGINS, ALLOWED_HOSTS
```

## installing
```
git clone https://gitlab.com/maria.zadorina/short_url.git
cd short_url
sudo chmod ugo+x init.sh
sudo ./init.sh
```
