import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "short_url.settings")
app = Celery("short_url")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
