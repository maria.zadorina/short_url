import pytest
import requests

from apps.core.models import Url


@pytest.mark.django_db
def test_post_data_without_short_url(request, url_data_without_short_url):
    response = requests.post(
        request.config.getini("api_endpoint"),
        json=url_data_without_short_url,
    ).json()
    assert isinstance(response, dict) is True
    assert response.get("full_url") == url_data_without_short_url.get("full_url")
    assert response.get("short_url") != ""


@pytest.mark.django_db
def test_post_data_with_short_url(request, url_data_with_short_url):
    response = requests.post(
        request.config.getini("api_endpoint"),
        json=url_data_with_short_url,
    ).json()
    assert isinstance(response, dict) is True
    assert response.get("full_url") == url_data_with_short_url.get("full_url")
    assert response.get("short_url") == url_data_with_short_url.get("short_url")


@pytest.mark.django_db
def test_post_data_check_double(
    request,
    url_data_with_short_url,
):
    response = requests.post(
        request.config.getini("api_endpoint"),
        json=url_data_with_short_url,
    ).json()
    assert isinstance(response, dict) is True
    assert response.get("full_url") == url_data_with_short_url.get("full_url")
    assert response.get("short_url") == url_data_with_short_url.get("short_url")
    response = requests.post(
        request.config.getini("api_endpoint"),
        json=url_data_with_short_url,
    ).json()
    assert isinstance(response.get("short_url"), list) is True
    assert len(response.get("short_url")) == 1
    assert response.get("short_url")[0] == "This field must be unique."


@pytest.mark.django_db
def test_get_urls(request, creating_url):
    assert isinstance(creating_url, Url) is True
    response = requests.get(request.config.getini("api_endpoint")).json()
    assert isinstance(response, dict) is True
    assert isinstance(response.get("urls"), list) is True


@pytest.mark.django_db
def test_counter(request, url_data_with_session_key, creating_url):
    requests.post(
        request.config.getini("api_endpoint"),
        json=url_data_with_session_key,
    ).json()
    response = requests.get(creating_url.short_url).json()
    assert isinstance(response, dict) is True
    assert response.get("full_url") != ""
    # todo: check celery task


@pytest.mark.django_db
def test_valid_url(request, invalid_data):
    response = requests.post(
        request.config.getini("api_endpoint"),
        json=invalid_data,
    ).json()
    assert isinstance(response.get("short_url"), list) is True
    assert len(response.get("short_url")) == 1
    assert response.get("short_url")[0] == "Enter a valid URL."
