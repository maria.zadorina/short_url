import secrets
import string

import pytest

from apps.core.models import Url
from short_url.settings import BASE_URL


def pytest_addoption(parser):
    parser.addini("api_endpoint", "path api for test")


@pytest.fixture
def sub_part():
    return "".join(
        secrets.choice(string.ascii_uppercase + string.digits) for _ in range(7)
    )


@pytest.fixture
def url_data_without_short_url():
    return {"full_url": "https://www.django-rest-framework.org/api-guide/"}


@pytest.fixture
def url_data_with_short_url(sub_part):
    return {
        "full_url": "https://www.django-rest-framework.org/api-guide/",
        "short_url": f"{BASE_URL}/{sub_part}/",
    }


@pytest.fixture
def url_data_with_session_key(sub_part):
    return {
        "full_url": "https://www.django-rest-framework.org/api-guide/",
        "short_url": f"{BASE_URL}/{sub_part}/",
        "session_key": "ud41k70co03obdprteerhmpufi5n2afq",
    }


@pytest.fixture
def creating_url(url_data_with_session_key):
    return Url.objects.create(**url_data_with_session_key)


@pytest.fixture
def invalid_data():
    return {
        "full_url": "https://www.django-rest-framework.org/api-guide/",
        "short_url": "ud41k70co03obdprteerhmpufi5n2afq",
    }
