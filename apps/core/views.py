import logging
import secrets
import string

from django.core.cache import cache
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.core.models import Url
from apps.core.serializers import UrlSerializer
from apps.core.tasks import clicked_task
from short_url.settings import BASE_URL


log = logging.getLogger(__name__)

ONE_DAY = 60 * 60 * 24


def get_session_key(request) -> str:
    session_key = request.session._session_key
    if not session_key:
        request.session.create()
        session_key = request.session._session_key
    return session_key


class ShortUrlApi(APIView):
    def get(self, request):
        session_key = get_session_key(request)
        urls = Url.objects.filter(session_key=session_key)
        return Response(
            {
                "urls": UrlSerializer(
                    urls,
                    fields=("full_url", "short_url", "count_transitions"),
                    many=True,
                ).data,
            },
        )

    def post(self, request):
        data = request.data
        data.update(session_key=get_session_key(request))

        if not data.get("short_url"):
            sub_part = "".join(
                secrets.choice(string.ascii_uppercase + string.digits) for _ in range(7)
            )
            data.update(short_url=f"{BASE_URL}/{sub_part}/")

        log.debug(f"ShortUrlApi; post; {data=}")

        serializer = UrlSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        cache.set(data.get("short_url"), data.get("full_url"), ONE_DAY)
        return Response(serializer.data)


class ShortUrlCounter(APIView):
    def get(self, request, *args, **kwargs):
        short_url = f"{BASE_URL}{request.path}"
        full_url = cache.get(short_url)
        if full_url:
            clicked_task.delay(short_url)
            return Response({"full_url": full_url})
        else:
            return Response(f"{short_url} - url does not exist")
