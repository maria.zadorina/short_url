import logging
from time import time

from celery import shared_task
from django.core.cache import cache

from apps.core.models import Url


log = logging.getLogger(__name__)


@shared_task
def clicked_task(short_url: str):
    log.info("clicked_task; Task started;")
    start_time = time()

    try:
        url = Url.objects.get(short_url=short_url)
        url.clicked()
    except Url.DoesNotExist:
        log.error(f"{short_url} does not exist")

    log.info(
        f"clicked_task; Task completed; Elapsed time {(time() - start_time)} seconds;",
    )


@shared_task
def clear_cash():
    log.info("clear_cash; Task started;")
    start_time = time()

    cache.clear()

    log.info(
        f"clear_cash; Task completed; Elapsed time {(time() - start_time)} seconds;",
    )


@shared_task
def clear_urls():
    log.info("clear_urls; Task started;")
    start_time = time()

    count_urls = Url.objects.all().delete()

    log.info(
        f"clear_urls; Task completed; Deleted {count_urls[0]} urls; "
        f"Elapsed time {(time() - start_time)} seconds;",
    )
