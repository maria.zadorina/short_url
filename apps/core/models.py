from django.db import models


class Url(models.Model):
    full_url = models.URLField(
        "Полный URL-адрес",
        help_text="full_url | Полный URL-адрес "
        "(Uniform Resource Locator – унифицированный указатель информационного ресурса)",
    )
    short_url = models.URLField(
        "Укороченный URL-адрес",
        unique=True,
        help_text="short_url | Укороченный URL-адрес",
    )
    count_transitions = models.IntegerField(
        "Количество переходов",
        default=0,
        help_text="count_transitions | Количество переходов по URL-адресу",
    )
    session_key = models.CharField(
        "Идентификатор сессии",
        max_length=32,
        help_text="session_key | Идентификатор сессии пользователя",
    )

    updated = models.DateTimeField("Обновлено", auto_now=True)
    created = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        verbose_name = "Url-адрес"
        verbose_name_plural = "Url-адреса"

    def __str__(self):
        return f"Короткий URL-адрес: {self.short_url}"

    def clicked(self):
        self.count_transitions += 1
        self.save()
