# Generated by Django 4.1.7 on 2023-03-15 06:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='url',
            name='short_url',
            field=models.URLField(help_text='short_url | Укороченный URL-адрес', unique=True, verbose_name='Укороченный URL-адрес'),
        ),
    ]
