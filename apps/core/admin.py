from django.contrib import admin

from .models import Url


@admin.register(Url)
class UrlAdmin(admin.ModelAdmin):
    list_display = ("full_url", "short_url", "count_transitions", "session_key")
    readonly_fields = ("session_key", "updated", "created")
    search_fields = ("session_key",)
    show_full_result_count = False
