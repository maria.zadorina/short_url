from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from apps.core.models import Url


class Command(BaseCommand):
    help = "Начальное заполнение базы данных"

    def handle(self, *args, **options):
        self.stdout.write("Старт инициализации базы данных;")
        models = (User, Url)
        for model in models:
            print(model.objects.all().delete())
        User.objects.create_superuser(username="admin", password="admin")
