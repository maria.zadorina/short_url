from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.core.models import Url


class UrlSerializer(serializers.ModelSerializer):
    full_url = serializers.URLField()
    short_url = serializers.URLField(
        validators=[UniqueValidator(queryset=Url.objects.all())],
    )
    count_transitions = serializers.IntegerField(default=0)
    session_key = serializers.CharField(max_length=32)
    updated = serializers.DateTimeField(read_only=True)
    created = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Url
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop("fields", None)

        # Instantiate the superclass normally
        super().__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)

    def create(self, validated_data):
        return Url.objects.create(**validated_data)
